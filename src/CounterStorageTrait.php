<?php

namespace A4Sex;

trait CounterStorageTrait
{
    public function increment($key, int $expiry = 0)
    {
        $counter = $this->prefix() . "counter_$key";
        return $this->memcached->increment($counter, 1, 0, $expiry);
    }

    public function decrement($key, int $expiry = 0)
    {
        $counter = $this->prefix() . "counter_$key";
        return $this->memcached->decrement($counter, 1, 0, $expiry);
    }

    public function count($key)
    {
        $counter = $this->prefix() . "counter_$key";
        return $this->load($counter, 0);
    }

    abstract public function prefix();

    abstract public function save($key, $value, $expires = null);

    abstract public function load($key, $default = null);
}
