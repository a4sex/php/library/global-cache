SHELL := /bin/bash

test: export APP_ENV=test
test:
	php vendor/phpunit/phpunit/phpunit --testdox --configuration phpunit.xml.dist

.PHONY: test
