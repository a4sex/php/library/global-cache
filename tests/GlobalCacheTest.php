<?php

namespace A4Sex\Tests;

use A4Sex\GlobalCache;
use PHPUnit\Framework\TestCase;

class GlobalCacheTest extends TestCase
{
    public GlobalCache $object;

    protected function setUp(): void
    {
//        parent::setUp();
        $this->object = new GlobalCache('memcached://localhost');
    }

    public function testSave()
    {
        $this->object->save('test', '123');
        self::assertEquals('123', $this->object->load('test'));
    }

    public function testSaveBool()
    {
        $this->object->delete('bool');
        self::assertFalse($this->object->load('bool'));
        $this->object->save('bool', true);
        self::assertTrue($this->object->load('bool'));
        $this->object->delete('bool');
    }

    public function testDelete()
    {
        $this->object->save('test', '123');
        $this->object->delete('test');
        self::assertFalse( $this->object->load('test'));
    }

    public function testPrefix()
    {
        self::assertEquals('global_cache_5oHGXK9NemOo__', $this->object->prefix());
    }

}
